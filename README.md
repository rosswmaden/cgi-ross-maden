##Technical Test - CGI 

I filled in this README as I went - total time spent on this test was about 2.5 hours. Hope to hear from you soon and apolagies for not getting this to you sooner - life has been quite hectic the past week!

## Problems Attempted & Notes

### Problem 1 – Divisor
1. Write a Java class with a method that will test if a number (X) is a divisor (factor) of 35. The method should return a boolean.

- Found in the `DivisorUtility` class. Tests are in `DivisorUtililityParamaterizedTest`

2. Next modify the method to test all the numbers up to and including X. Print out the number if it is a divisor.

- Also found in the `DivisorUtility` class. Tests are in `DivisorUtilityTest`

### Problem 2 - Collision Detection

1. Imagine a room full of balloons that are floating around and may collide with one another. A balloon object has been provided for you which contains a method isCollide() to detect these collisions. The method accepts another balloon object to test against. Write a method that will determine which balloons are colliding and if they do, display this back to the user.
2. If you have managed to solve the problem, please think about how you could make this method more efficient.

*I made a bold assumption here that I was able to modify the classes provided slightly - for the solution I wanted to go for initially to avoid the n squared complexity.*
*If I wasn't able to modify Location to add in a getter - then would of likely had to a few more steps to do a lookup map , but can discuss the 0(N) time approach if needed*

- Added a `Room` helper object to hold some balloons - thought it would be over-engineering but I had to resist adding an interface for `BalloonContainer`
- Solution was along the lines of - I iterate through the balloons once per request and build a distribution map of location -> list of balloons. If I get more than one in that list, then I know there are collisions.

*There are downsides to this approach - as the GroupingBy logic to construct the distribution map is "aware" of the collision logic ( knowing that purely the integers are compared"*

- Found in the `Room` class mostly - with tests in `RoomTest`

### Problem 3 – High Numbers

1. Write some code to find the second largest number in an array, next, generalise this for the nth highest number

- Implemented the easiest solution which is around sorting in `HighNumberUtility` under the method `getNthLargestNumberWithSorting`
- Realised this is solvable in average time O(N) by using QuickSelect. I'll be completely honest here and say that I did end up copying my solution that I wrote back in University. I want to be honest as I'm very much against re-inventing the wheel where ever I can. Copied over the old code and tidied it up.
 

### Problem 4  - Palindrome Discussion

While the powerpoint does say to avoid simple testing methods, I'm going to include everything I can think of including simple for completeness sake.

1. Around the unit doing the test - add a custom timer or use a Guava Stopwatch to test an incredibly large number of each methods invocations. From this data we could calculate average execution time. However it would tell us nothing of efficiency.
2. Have a JVM running each of the different methods and attach a profile to the JVM instance ( one I'm familiar with is JVisualVM ).  If a request is made to get the Palindrome we can view the time spent in each method ( not raw time - CPU time ) which is a great indicator of the speediness.
3. In terms of efficiency of resources - JVisualVM would also allow for retrieval of heap snapshots to view which is having a greater memory impact.

### Problem 5 - Discussion Points

*Not sure how much I'me meant to write here - so keeping it light in-case these are topics to be discussed in person if I were to make it to the next stage.*

#### Naming conventions, why do we have them?

Naming conventions are *incredibly* important for any development project,  they provide a very quick and visual method of breaking down what a class/file does without having to dig into any of the code.

- Does my class end with Dao? It's going to be accessing data.
- Does my class end with utility? Then I know there isn't any business logic in here.
- Does this variable start with mock? Then I know it's not real!

While each team in my experience will have their own naming conventions - a great benefit is that many are industry-wide. Meaning naming standards allow new developers to hit the ground faster.

#### Design Patterns, what are the most common patterns you use in your day to day job?

* Going to include 3 patterns I use day to day *

1. Builder pattern - I'm a great believer of having most if not all domain objects built through builders. It's a great way to abstract potentially complex construction processes into a single location. 
2. Observer pattern - for some of my current systems integration tests, I latch on additional observers of important system events in order to assert different stages of the process. While the observer pattern has allowed us to make critical assertions in a non-intrusive way - one of the downsides of using Observers is that it isn't always clear "what" is listening to the events you send.
3. Factory pattern - on the current system that I lead development for, the systems behaviour is driven off of a core set of enums. There is a suite of factories in different micro-services which know how to translate these enums into abstracted logic.

#### We don’t need Architects they’re just an over head! – argue for this point

Having an architect in a team can actually lead to something which all teams should strive to avoid - a solo'd decision making process. In my opinion the onus of designing/implementing and owning future and current systems lies with the team as a whole ( not just developers! ).  This helps to ensure that all team members regardless of level and experience can bring ideas to the table in a safe manner.

A modern alternative to an architect is having technical discussions within teams a few times a sprint , where the future direction can be discussed and improved upon iteratively.





