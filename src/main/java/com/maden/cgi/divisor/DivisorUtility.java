package com.maden.cgi.divisor;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class DivisorUtility {

    private DivisorUtility() {
        throw new IllegalStateException("Please don't instantiate utility classes!");
    }

    /**
     * Returns whether or not a given number has the given potential divisor ( factor )
     *
     * @param number           Number to validate the potentialDivisor against
     * @param potentialDivisor PotentialDivisor to check for.
     * @return True if <code>potentialDivisor</code> is a factor of <code>number</code>
     */
    public static boolean isDivisor(int number, int potentialDivisor) {
        return number % potentialDivisor == 0;
    }


    /**
     * Get all divisors between <code>startInclusive</code> and <code>endInclusive</code>
     *
     * @param number         Number to validate the potentialDivisor against
     * @param startInclusive Value to start looking for divisors.
     * @param endInclusive   Value to stop looking for divisors.
     */
    public static List<Integer> getDivisors(int number, int startInclusive, int endInclusive) {

        if (number == 0) {
            throw new IllegalArgumentException("Please don't ask me to find divisors of 0!");
        }

        if (startInclusive > endInclusive || endInclusive < startInclusive) {
            throw new IllegalArgumentException("Please ensure the range is in the correct order!");
        }

        return IntStream.rangeClosed(startInclusive, endInclusive)
                .filter(value -> isDivisor(number, value))
                .boxed()
                .collect(Collectors.toList());


    }

    /**
     * Print out all divisors between <code>startInclusive</code> and <code>endInclusive</code>
     *
     * @param number         Number to validate the potentialDivisor against
     * @param startInclusive Value to start looking for divisors.
     * @param endInclusive   Value to stop looking for divisors.
     */
    public static void printDivisors(int number, int startInclusive, int endInclusive) {

        List<Integer> divisors = getDivisors(number, startInclusive, endInclusive);
        String resultString = divisors.stream()
                .map(Objects::toString)
                .collect(Collectors.joining(","));

        System.out.println(String.format("When looking for divisors of %s between %s and %s - found %s to be divisors", number, startInclusive, endInclusive, resultString));

    }

}
