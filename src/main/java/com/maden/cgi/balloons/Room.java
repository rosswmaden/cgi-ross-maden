package com.maden.cgi.balloons;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Room {

    private List<Balloon> balloons;

    public Room(List<Balloon> balloons) {
        this.balloons = balloons;
    }

    public Room() {
        this(new ArrayList<>());
    }

    public void addBalloon(Balloon balloon) {
        this.balloons.add(balloon);

    }

    public List<Balloon> getBalloons() {
        return new ArrayList<>(balloons);
    }

    public List<Balloon> getCollidingBalloons() {
        return balloons.stream()
                .collect(Collectors.groupingBy(balloon -> balloon.getLocation().getWhere()))
                .entrySet()
                .stream()
                .filter(balloonMapEntry -> balloonMapEntry.getValue().size() > 1)
                .map(Map.Entry::getValue)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    public void printCollidingBalloons() {
        getCollidingBalloons()
                .stream()
                .forEach(System.out::println);
    }
}
