package com.maden.cgi.high;

import java.util.Arrays;
import java.util.stream.IntStream;

public final class HighNumberUtility {

    private HighNumberUtility() {
        throw new IllegalStateException("Please don't instantiate utility classes!");
    }


    /**
     * Given an input number array  - find the n'th largest number. This method uses sorting as it's the easiest solution.
     *
     * @param numberArray Array to check
     * @param n           Nth largest value to find.
     * @return Nth largest value in the array.
     */
    public static int getNthLargestNumberWithSorting(int[] numberArray, int n) {

        if (numberArray == null) {
            throw new IllegalArgumentException("Please give me an actual array to check against!");
        }

        if (n < 1 || n > numberArray.length) {
            throw new IllegalArgumentException("Please ensure that n is populated and is lower than the input array size.");
        }

        return Arrays.stream(numberArray)
                .sorted()
                .skip(numberArray.length - n)
                .findFirst()
                .getAsInt();
    }

    /**
     * Given an input number array  - find the n'th largest number. This method uses quick-select to reduce the lookup time from sorting ( logn ) to be best-case for selection at ( n )
     *
     * @param numberArray Array to check
     * @param n           Nth largest value to find.
     * @return Nth largest value in the array.
     */
    public static int getNthLargestNumberWithQuickSelect(int[] numberArray, int n) {
        if (numberArray == null) {
            throw new IllegalArgumentException("Please give me an actual array to check against!");
        }

        if (n < 1 || n > numberArray.length) {
            throw new IllegalArgumentException("Please ensure that n is populated and is lower than the input array size.");
        }

        return quickSelectFromArray(numberArray, 0, numberArray.length - 1, numberArray.length - n);
    }

    private static int quickSelectFromArray(int[] numberArray, int lowerBound, int upperBound, int wantedIndex) {
        int pivotPosition = pivotArray(numberArray, lowerBound, upperBound);
        if (pivotPosition - lowerBound == wantedIndex) {
            return numberArray[pivotPosition];
        }
        if (pivotPosition - lowerBound > wantedIndex) {
            //Our index is in the lower half of the input array.
            return quickSelectFromArray(numberArray, lowerBound, pivotPosition - 1, wantedIndex);
        } else {
            //Our index is in the upper half of the input array.
            return quickSelectFromArray(numberArray, pivotPosition + 1,
                    upperBound, wantedIndex - pivotPosition + lowerBound - 1);
        }

    }


    /**
     * Pivot a given array - re-ordering the input array so that it results in the following.
     *
     * MRE - Most Right Element
     *
     * Elements < MRE , MRE , Elements > MRE
     *
     * @param inputArray Array to be pivoted.
     * @param startIndex Index to start from on the array.
     * @param pivotPoint Index to use as the pivot point.
     * @return Index of pivotPoint post-pivot.
     */
    private static int pivotArray(int[] inputArray, int startIndex, int pivotPoint) {

        int pivotElement = inputArray[pivotPoint];

        int[] lowerValues = IntStream.range(startIndex, pivotPoint)
                .filter(i -> inputArray[i] < pivotElement)
                .map(i -> inputArray[i])
                .toArray();

        int[] rightArr = IntStream.range(startIndex, pivotPoint)
                .filter(i -> inputArray[i] > pivotElement)
                .map(i -> inputArray[i])
                .toArray();

        System.arraycopy(lowerValues, 0, inputArray, startIndex, lowerValues.length);
        inputArray[lowerValues.length + startIndex] = pivotElement;
        System.arraycopy(rightArr, 0, inputArray, startIndex + lowerValues.length + 1,
                rightArr.length);

        return startIndex + lowerValues.length;
    }
}
