package com.maden.cgi.divisor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class DivisorUtilityParameterizedTest {

    @Parameterized.Parameters
    public static Collection<Object[]> parametersForValidateFactors() {
        return Arrays.asList(new Object[][]{
                {8, 1, true},
                {8, 2, true},
                {8, 3, false},
                {8, 4, true},
                {8, 5, false},
                {8, 6, false},
                {8, 7, false},
                {8, 8, true},
                {8, 9, false},
                {8, 10, false}
        });
    }

    @Parameterized.Parameter(0)
    public int number;

    @Parameterized.Parameter(1)
    public int potentialFactor;

    @Parameterized.Parameter(2)
    public boolean expectedOutcome;

    @Test
    public void validateFactors() {
        boolean outcome = DivisorUtility.isDivisor(number, potentialFactor);
        assertEquals(expectedOutcome, outcome);
    }

}