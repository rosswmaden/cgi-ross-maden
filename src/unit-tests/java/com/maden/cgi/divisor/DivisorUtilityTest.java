package com.maden.cgi.divisor;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class DivisorUtilityTest {

    @Test
    public void validateGetFactorsHousesSameResults() {

        int number = 8;
        int startInclusive = 1;
        int endInclusive = 10;

        List<Integer> divisors = DivisorUtility.getDivisors(number, startInclusive, endInclusive);
        assertEquals(4, divisors.size());
        assertTrue(divisors.contains(1));
        assertTrue(divisors.contains(2));
        assertTrue(divisors.contains(4));
        assertTrue(divisors.contains(8));
    }
}