package com.maden.cgi.high;

import org.junit.Test;

import static com.maden.cgi.high.HighNumberUtility.getNthLargestNumberWithQuickSelect;
import static com.maden.cgi.high.HighNumberUtility.getNthLargestNumberWithSorting;
import static org.junit.Assert.assertEquals;

public class HighNumberUtilityTest {

    @Test(expected = IllegalArgumentException.class)
    public void testThrowsIllegalArgumentExceptionOnBadInputNth() {
        getNthLargestNumberWithSorting(new int[]{1}, -2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testThrowsIllegalArgumentExceptionOnBadInputArray() {
        getNthLargestNumberWithSorting(null, -2);
    }

    @Test
    public void singleInputArray() {
        int[] vals = {1};
        int nthLargestNumberWithSorting = getNthLargestNumberWithSorting(vals, 1);
        assertEquals(1, nthLargestNumberWithSorting);
    }

    @Test
    public void multipleInputArrayWithSortingApproach() {
        int[] vals = {1,3,6,9,1,12};
        assertEquals(12, getNthLargestNumberWithSorting(vals, 1));
        assertEquals(9, getNthLargestNumberWithSorting(vals, 2));
        assertEquals(6, getNthLargestNumberWithSorting(vals, 3));
        assertEquals(3, getNthLargestNumberWithSorting(vals, 4));
        assertEquals(1, getNthLargestNumberWithSorting(vals, 5));
        assertEquals(1, getNthLargestNumberWithSorting(vals, 6));
    }

    @Test
    public void multipleInputArrayWithQuickSelectApproach() {
        int[] vals = {1,3,6,9,1,12};
        assertEquals(12, getNthLargestNumberWithQuickSelect(vals, 1));
        assertEquals(9, getNthLargestNumberWithQuickSelect(vals, 2));
        assertEquals(6, getNthLargestNumberWithQuickSelect(vals, 3));
        assertEquals(3, getNthLargestNumberWithQuickSelect(vals, 4));
        assertEquals(1, getNthLargestNumberWithQuickSelect(vals, 5));
        assertEquals(1, getNthLargestNumberWithQuickSelect(vals, 6));
    }

    @Test
    public void singleInputArrayWithQuickSelectApproach() {
        int[] vals = {1};
        int nthLargestNumberWithSorting = getNthLargestNumberWithSorting(vals, 1);
        assertEquals(1, nthLargestNumberWithSorting);
    }
}