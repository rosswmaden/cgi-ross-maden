package com.maden.cgi.balloons;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.junit.Assert.*;

public class RoomTest {

    private Room room;

    @Before
    public void setUp() throws Exception {
        room = new Room();
    }

    @After
    public void tearDown() throws Exception {
        room = null;
    }

    private final Random locationSelector = new Random();


    @Test
    public void getCollidingBalloonsWithOneBalloon() {

        int location = locationSelector.nextInt(20) + 1;
        room.addBalloon(new Balloon(UUID.randomUUID().toString(), new Location(location)));

        assertEquals(1, room.getBalloons().size());
        List<Balloon> collidingBalloons = room.getCollidingBalloons();
        assertTrue(collidingBalloons.isEmpty());
    }

    @Test
    public void getCollidingBalloonsWithTwoInSameLocation() {

        int location = locationSelector.nextInt(20) + 1;

        room.addBalloon(new Balloon("BALLOON_1", new Location(location)));
        room.addBalloon(new Balloon("BALLOON_2", new Location(location)));

        assertEquals(2, room.getBalloons().size());
        List<Balloon> collidingBalloons = room.getCollidingBalloons();
        assertEquals(2, collidingBalloons.size());
        assertTrue(collidingBalloons.stream().anyMatch(balloon -> balloon.getName().equals("BALLOON_1")));
        assertTrue(collidingBalloons.stream().anyMatch(balloon -> balloon.getName().equals("BALLOON_2")));
    }

    @Test
    public void getCollidingBalloonsWithTwoInSameLocationAndOneInAnotherLocation() {

        int collidingLocation = locationSelector.nextInt(20) + 1;
        room.addBalloon(new Balloon("BALLOON_1", new Location(collidingLocation)));
        room.addBalloon(new Balloon("BALLOON_2", new Location(collidingLocation)));

        int nonCollidingLocation = ++collidingLocation;
        room.addBalloon(new Balloon("BALLOON_3", new Location(nonCollidingLocation)));

        assertEquals(3, room.getBalloons().size());
        List<Balloon> collidingBalloons = room.getCollidingBalloons();
        assertEquals(2, collidingBalloons.size());
        assertTrue(collidingBalloons.stream().anyMatch(balloon -> balloon.getName().equals("BALLOON_1")));
        assertTrue(collidingBalloons.stream().anyMatch(balloon -> balloon.getName().equals("BALLOON_2")));
        assertFalse(collidingBalloons.stream().anyMatch(balloon -> balloon.getName().equals("BALLOON_3")));
    }
}